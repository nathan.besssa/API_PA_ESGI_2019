'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/guides/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async destroy(ctx) {
    const { voyages, etapes, picture } = await strapi.plugins['users-permissions'].services.user.fetch({id: ctx.state.user.id});
    if(picture){
      ctx.params = {
        _id: picture.id,
        id: picture.id
      };

      if(picture.id)
        await strapi.plugins['upload'].controllers.upload.destroy(ctx);
    }

    voyages.map(voyage => {
      if(voyage.picture) {
        ctx.params = {
          _id: voyage.picture.id,
          id: voyage.picture.id
        };
        strapi.plugins['upload'].controllers.upload.destroy(ctx);
      }
      strapi.services.voyage.delete({id: voyage.id});
    });

    etapes.map(etape => {
      etape.pictures.map(picture => {
        ctx.params = {
          _id: picture.id,
          id: picture.id
        };
        strapi.plugins['upload'].controllers.upload.destroy(ctx);
      });

      strapi.services.etape.delete({id: etape.id});
    });
    return strapi.plugins['users-permissions'].services.user.remove({id: ctx.state.user.id});
  }
};
