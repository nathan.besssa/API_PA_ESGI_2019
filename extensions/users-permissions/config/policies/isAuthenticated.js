'use strict';

module.exports = async (ctx, next) => {
  if(ctx.state.user.id != ctx.params._id){
    console.log('oui')
    return ctx.unauthorized('Forbidden access');
  }
  ctx.params.id = ctx.params._id
  await next();
};
