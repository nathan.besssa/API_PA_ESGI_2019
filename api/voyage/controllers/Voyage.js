'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/guides/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  create(ctx) {
    ctx.request.body.user = ctx.state.user.id;
    const { start, end } = ctx.request.body;
    if(new Date(start) > new Date(end))
      return ctx.badRequest('Start must be before end');
    return strapi.services.voyage.create(ctx.request.body);
  },
  async update(ctx) {
    const recordUserId = await Promise.resolve(strapi.services.voyage.findOne(ctx.params).then(res => res.attributes.user));
    if(recordUserId !== ctx.state.user.id)
      return ctx.unauthorized('Forbidden access');
    return strapi.services.voyage.update(ctx.params, ctx.request.body);
  },
  async delete(ctx) {
    const recordUserId = await Promise.resolve(strapi.services.voyage.findOne(ctx.params).then(res => res.attributes.user));

    if(recordUserId !== ctx.state.user.id)
      return ctx.unauthorized('Forbidden access');

    const params = ctx.params;
    const pictureToDeleteId = await strapi.services.voyage.findOne(ctx.params).then(res => res.relations.picture.attributes.upload_file_id);

    ctx.params = {
      _id: pictureToDeleteId,
      id: pictureToDeleteId
    };
    strapi.plugins['upload'].controllers.upload.destroy(ctx);

    return strapi.services.voyage.delete(params);
  }
};
