'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/guides/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async create(ctx) {
    const recordUserId = await Promise.resolve(strapi.services.voyage.findOne({id: ctx.request.body.voyage}).then(res => res.attributes.user));
    if(recordUserId !== ctx.state.user.id)
      return ctx.unauthorized('Forbidden access');
    ctx.request.body.user = ctx.state.user.id;
    return strapi.services.etape.create(ctx.request.body);
  },
  async update(ctx) {
    const recordUserId = await Promise.resolve(strapi.services.etape.findOne(ctx.params).then(res => res.attributes.user));
    if(recordUserId !== ctx.state.user.id)
      return ctx.unauthorized('Forbidden access');
    return strapi.services.etape.update(ctx.params, ctx.request.body);
  },
  async delete(ctx) {
    const recordUserId = await Promise.resolve(strapi.services.etape.findOne(ctx.params).then(res => res.attributes.user));
    if(recordUserId !== ctx.state.user.id)
      return ctx.unauthorized('Forbidden access');

    const params = ctx.params;
    const picturesToDelete = await strapi.services.etape.findOne(ctx.params).then(res => res.relations.pictures.models);
    console.log(picturesToDelete);

    picturesToDelete.map(picture => {
      ctx.params = {
        _id: picture.attributes.upload_file_id,
        id: picture.attributes.upload_file_id
      };
      strapi.plugins['upload'].controllers.upload.destroy(ctx);
    });

    return strapi.services.etape.delete(params);
  }
};
