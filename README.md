# API Tripadlowcost

> Strapi project

## Prerequisite

- Node version `>= 10.x.x`
- Mysql server (prefered `5.6`) on localhost (or change develop config)
- Mysql user root without password (or change develop config)
- Create database named `tripadlowcost`

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# start dev server
$ npm run start # Or yarn run start
```

Admin panel can be accessed on `localhost:1337/admin`

